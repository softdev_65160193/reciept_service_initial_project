/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author waran
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer : cs.getcustomers()){
            System.out.println(customer);
        }
        System.out.println(cs.getByTel("0123456789"));
        Customer cus1 = new Customer("kob","0987654321");
        cs.addNew(cus1);
        for(Customer customer : cs.getcustomers()){
            System.out.println(customer);
        }
        Customer delcus = cs.getByTel("0987654321");
        delcus.setTel("0100000001");
        cs.update(delcus);
        System.out.println("After Updated");
        for(Customer customer : cs.getcustomers()){
            System.out.println(customer);
        }    
        cs.delete(delcus);
        for(Customer customer : cs.getcustomers()){
            System.out.println(customer);
        }
    }
            
}
